#!/bin/bash

GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0;0m'

# Creates host file with IP to EC2 instance
printf "${BLUE}[QUESTION] What is the IP address of the EC2 instance?${NC} "
read EC2_IP

printf "${BLUE}[QUESTION] What is the Username for the EC2 instance?${NC} "
read EC2_UNAME

sed provisionning/inventory/hosts.dist -e s/%IP_TO_EC2_INSTANCE%/${EC2_IP}/ > provisionning/inventory/hosts.ip
sed provisionning/inventory/hosts.ip -e s/%USERNAME_EC_INSTANCE%/${EC2_UNAME}/ > provisionning/inventory/hosts
rm provisionning/inventory/hosts.ip

printf "${GREEN}[OK] Host file updated.${NC}\n\n"