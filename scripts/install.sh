#!/bin/bash

ORANGE='\033[0;33m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0;0m'

# Tests if ansible virtualenv already exists
if [ -d pyenv ]; then
    printf "${ORANGE}[WARNING] Ansible env already setup.${NC}\n"
else
    # Set up virtualenv
    printf "${BLUE}[INFO] Installing Python virtual environment...${NC}\n"
    virtualenv -qp python3.6 pyenv
    printf "${GREEN}[OK] Python virtual installed!${NC}\n\n"

    # Install Python dependencies
    printf "${BLUE}[INFO] Installing dependencies...${NC}\n"
    source pyenv/bin/activate
    pip install -qr requirements.txt
    printf "${GREEN}[OK] Dependencies installed!${NC}\n\n"
fi

./scripts/update_ec2_ip.sh
printf "${BLUE}[INFO] Should the IP change, simply run ./scripts/update_ec2_ip.sh.${NC}\n\n"

# Final words
printf "You can now run ${GREEN}\"source pyenv/bin/activate\"${NC} and use Ansible. Test with ${GREEN}\"make ping\"${NC}.\n\n"