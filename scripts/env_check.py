import os
from dotenv import load_dotenv

expected_root_tokens = {
    'front': 'front_project_root',
    'back': 'back_project_root'
}

def main():
    load_dot_env()

    roots = {
        'front': os.getenv('FRONT'),
        'back': os.getenv('BACK')
    }

    errors = 0

    for project, path in roots.items():
        expected_path_to_root = os.path.join(roots[project], expected_root_tokens[project])

        if not os.path.isfile(expected_path_to_root):
            print('[ERROR] Root token not found at %s for %s' % (expected_path_to_root,project))
            errors = errors +1

    if errors == 0:
        print("You're all setup, happy coding!")
    else:
        print("[ERROR] It seems some paths are misconfigured. Please check your .env file.")

def load_dot_env():
    dotenv_path = os.path.join(os.path.dirname(__file__), '..', '.env')

    if not os.path.isfile(dotenv_path):
        print("[ERROR] .env file not found. Looked in: %s" % dotenv_path)

        exit()

    load_dotenv(dotenv_path)

if "__main__" == __name__:
    main()