.PHONY: ping provision up down

ping:
	@ansible all -m ping

provision:
	@ansible-playbook provisionning/playbook.yaml --ask-become-pass

up:
	@docker-compose up -d; \
	echo "\nDjango: http://localhost:8000"; \
	echo "React: http://localhost:3000\n"

down:
	@docker-compose down --remove-orphans

reload: down up
