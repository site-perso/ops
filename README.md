# My Personnal Website - OPS

This repo is responsible to the environment creation (AWS, Ansible, Docker).

## Setup the development environment

The Docker compose volumes mounting has dependencies with other projects. The path to these projects must be defined in the `.env` file. Copy the `.env.dist` file to `.env` and adjust the FRONT and BACK variables so that they respectively point to root path for the front and back project. You can test if the setup is correct by running:
```bash
$ source pyenv/bin/activate
$ python scripts/env_check.py
```

Then run the Docker environment with:
```bash
$ docker-compose up -d
```

## Setup the AWS Instance

### Cloud formation

- Make sure there is a VPC with a default Subnet in the Availability Zone selected (will _propbably_ add that to the stack one day).
- Create a new Cloud Formation stack from [website.yaml](cloudformation/website.yaml)
- When prompted for the _InstanceHomePublicIp_, fill in 'xxx.xxx.xxx.xxx/32', (xxx.xxx.xxx.xxx being your home IP public address).
- Define provisionning _username_ and _password_.
- Have your public key ready.

### Provisionning

- From the project root dir, run the [install.sh](scripts/install.sh) script. You'll need the instance's **Elastic IP address**.
- Run `make ping` to test if Ansible manages to ping the instance properly.
- Run `make provision`. Provide the password you defined in CloudFormation when asked.
- Brace Yourself, Provisionning is coming.

## Todo
- [X] Script to test if .env file pathes are correct.
- [ ] Write the Docker-compose files.
- [ ] Write playbook to provision the instance.
- [ ] Add VPC to CloudFormation.
- [ ] Add RDS to CloudFormation.